# Gemnasium Maven Plugin changelog

## 0.7.0 / 2024-05-10

* update `maven-core` to `3.9.6`

## 0.6.0 / 2023-11-29

* use requiresDependencyCollection for multi-modules

## 0.5.1 / 2023-03-23

* updated `jackson-databind` to `2.14.2` and `maven-core` to `3.9.1`

## 0.5.0 / 2021-11-11

* updated `jackson-databind` to newest version

## 0.4.0 / 2020-03-11

* updates all dependencies to their latest versions
* removed features, code and documentation about Gemnasium projects, hosted on Gemnasium.com
* added publishing job, to automatically publish releases to maven central
* add the google java styleguide as mandatory checkstyle rules

## 0.3.0 / 2018-01-29

* Fix: don't fail the build when `create-project` goal can't create the gemnasium.properties file
* Feature: add a `dump-dependencies` goal that write project's dependencies to a json file

## 0.2.0 / 2017-11-02

* Fix upload payload (wrong parameter for filename)
* Improve generated dependency file content (send more data)

## 0.1.0 / 2017-11-02

Initial release
