<!--
  Use this template for your contribution to gemnasium-maven-plugin
-->

## What does this MR do?

<!-- Briefly describe what this MR is about -->

## Related issues

<!-- Link related issues below. -->

## Developer checklist

- [ ] Update CHANGELOG.md
- [ ] Update pom.xml with new plugin's version
- [ ] Update mentions of the new version in README.md

/label ~"Category:Dependency Scanning"
